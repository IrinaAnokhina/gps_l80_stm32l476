/*
 * FIFO_buf.h
 *
 *  Created on: 20 ���. 2018 �.
 *      Author: AnokhinaIrina
 */

#ifndef FIFO_BUF_H_
#define FIFO_BUF_H_

#define BUFFER_SIZE 2048
#define BUFFER_MASK (BUFFER_SIZE - 1)

unsigned char buffer[BUFFER_SIZE];
unsigned char tail;
unsigned char head;
unsigned char current;
unsigned char getData;
unsigned char InBuffer(unsigned char);
unsigned char OutBuffer();
unsigned char IndexNumber(void);
unsigned char GetData(void);


#endif /* FIFO_BUF_H_ */
