/*
 * GPS.h
 *
 *  Created on: 15 ����. 2018 �.
 *      Author: AnokhinaIrina
 */

#ifndef GPS_H_
#define GPS_H_

#define rx_size 256
#define rx_mask (rx_size - 1)
unsigned char rx[rx_size];
unsigned char ind_rx;

void GPS_run();
void parseGSV();
void parseRMC();
void clearBuf();
unsigned char is_gsv();
#endif /* GPS_H_ */
