/*
 * GPS.c
 *
 *  Created on: 15 ����. 2018 �.
 *      Author: AnokhinaIrina
 */
#include"GPS.h"
#include"FIFO_Buf.h"
#include"usart.h"
extern UART_HandleTypeDef huart3;
extern uint8_t new_head;
extern uint8_t token;
uint8_t temp_buf[64];
uint8_t temp_ind;
char longitude[16];
 char latitude[16];
 char date[8];
 char lat_d[1];
 char lon_d[1];
extern uint8_t gsv_count;
extern uint8_t rmc_count;
extern uint8_t gsv_in;
extern uint8_t rmc_in;
extern uint8_t ready_to_read;
char sat_c[5];
char time[16];
uint8_t i = 0;

enum
{
	TIME = 1,
	LAT,
	LAT_D,
	LONG,
	LONG_D,
	DATE
};

void GPS_run()
{
	if(ready_to_read)
	{
		//ready_to_read = 0;
	token = OutBuffer();
	   if(token == '$')
	   	   {
		   temp_ind = 0;
		   do
		  	{
		  	temp_buf[temp_ind++] = token;
		  	token = OutBuffer();
		  	}while(token != '*' && token != '\n' && token != '\r' && token != '$');
if(token == '$')
{
	head--;
	return;
}
if(token == '*')
	temp_buf[temp_ind++] = token;

		  if(!is_gsv())
		    return;
	   	   }
	   else
		   return;

	   if(gsv_in)
		   parseGSV();
	   else if(rmc_in)
		   parseRMC();
	   ready_to_read = 0;
	}
}

void parseGSV()
{
gsv_in = 0;
	sat_c[0] = temp_buf[11];
	sat_c[1] = temp_buf[12];
	//clearBuf();
}

void parseRMC()
{
	uint8_t mode_write = 0;
	uint8_t comma_count = 0;
	uint8_t i = 0;
	uint8_t skip = 0;
	temp_ind = 0;
	while(temp_buf[temp_ind] != '*' && temp_ind < 64)
	{
		if(temp_buf[temp_ind] == ',')
		{
			comma_count++;
			switch(comma_count)
			{
			case 1:
				mode_write = TIME;
				break;
			case 3:
				mode_write = LAT;
				break;
			case 4:
				mode_write = LAT_D;
				skip = 1;
				break;
			case 5:
				mode_write = LONG;
				skip = 0;
				break;
			case 6:
				mode_write = LONG_D;
				skip = 1;
				break;
			case 9:
				mode_write = DATE;
				skip = 0;
				break;
			default:
				mode_write = 0;
				break;
			}
			if(!skip)
			i = 0;
		}
		else
		{
			switch(mode_write)
			{
			case TIME:
time[i++] = temp_buf[temp_ind];
break;
			case LAT:
				latitude[i++] = temp_buf[temp_ind];
				break;
			case LAT_D:
				latitude[i++] = temp_buf[temp_ind];
				break;
			case LONG:
			longitude[i++] = temp_buf[temp_ind];
				break;
			case LONG_D:
				longitude[i++] = temp_buf[temp_ind];
				break;
			case DATE:
				date[i++] = temp_buf[temp_ind];
				break;
			default:
				break;
			}
		}
		temp_ind++;
	}
}

unsigned char is_gsv()
{
	char str1[5] = "GPGSV";
	char str0[5] = "GPRMC";
	uint8_t mode = 1;
	uint8_t i = 1, j = 0;

	for(; i < 3; i++, j++)
	{
		if(temp_buf[i] == str1[j])
			continue;
		else
			return 0;
	}
if(temp_buf[i] == str1[j])
	;
else if(temp_buf[i] == str0[j])
	mode = 0;
else
	return 0;
for(; i < 6; i++, j++)
{
	if(mode)
	{
		if(temp_buf[i] == str1[j])
					continue;
				else
					return 0;
	}
	else
	{
		if(temp_buf[i] == str0[j])
			continue;
		else
			return 0;
	}
}
	if(mode)
	{
		gsv_in = 1;
	gsv_count++;
	}
	else
	{
		rmc_in = 1;
		rmc_count++;
	}
	return 1;
}

void clearBuf()
{
	for(uint8_t k = 0; k < 32; k++)
		temp_buf[k] = 0;
}
