/*
 * FIFO_buf.c
 *
 *  Created on: 20 ���. 2018 �.
 *      Author: AnokhinaIrina
 */
#include"FIFO_BUF.h"

unsigned char InBuffer(unsigned char byte)
{
    tail &= BUFFER_MASK;
    buffer[tail++] = byte;
    return 1;
}

unsigned char OutBuffer()
{
	head &= BUFFER_MASK;
	return buffer[head++];
}

unsigned char IndexNumber(void)
{
    if (tail >= head)
    {
	return (tail - head);
    }
    else
    {
	return ((BUFFER_SIZE - head) + tail);
    }
}

unsigned char GetData(void)
{
    if(tail != head)
    	return 1;
    return 0;
}

